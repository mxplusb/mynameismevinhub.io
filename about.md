---
layout: page
title: About
permalink: /about/
---
###### About Me

I'm not terribly good at filling these sections out, so it's worth saying that I will do the best I can. I am a prior military systems engineer who became a quasi-programmer while trying to find my way through this ridiculously large field. I am very good at breaking things and telling people how not to do things. I work in various developmental languages, so I consider myself more programmer then developer these days...although web development is beyond me. I still don't understand how JavaScript can populate a floating box on a web page. It just doesn't make sense.

Anyway, I spent some time in the United States Marine Corps and through that, Afghanistan. After that, I spent some time at $employer1, $employer2, and now I work on various things for various people. In my spare time, I'm working on a statistics bot for Imgur to focus on my statistical analytics abilities.
