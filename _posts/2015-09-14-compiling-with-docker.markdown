---
layout: post
title: "Compiling with Docker"
date: Mon Sep 14 09:20:29 MST 2015
categories: docker
author: Mike Lloyd
comments: true
---

Recently I have started returning to C-based development to sharpen some of my core developmental skills, and to keep life interesting. While I like working with Python and Go, nothing quite beats C++ in some areas. One of the downsides to C++ and it's sister languages is the potential for long running compilations. Compiling in 2015 won't necessarily allow you time to go to BlizzCon, but it does still take time.

While I can compile locally with `make -j"$(nproc)"`, that is an old way of doing it. Let's add a new way of doing it by using Docker. Not only are we going to utilise container abstraction, but we're going to map your current working directory to a matching mount within a container, execute the compile, and save the binary. All with one command. The container is actually provided by the GCC team. 

What does compiling within a container give us? In my opinion, it gives the specific ability to compile either locally or remotely while using process separation and isolation. If your docker-machine configuration is local, then you can configure locally. If your docker-machine configuration is remote, on AWS/OpenStack/vCloud Air, then you can compile remotely through the Docker API. Depending on your shell profiles, you can change back and forth with ease. Compiling with containers gives much more flexibility as to scaling, isolation, and location.

We're going to compile two things; first we'll compile hello.cpp as a simple demonstration in a throwaway container, second will be an in-depth compile of GCC 5.1. While the inital compile is just a simple binary as a demonstration, the second compilation demonstrate a larger configuration. Below is the link to our larger compiling project.

[GCC 5.1](https://github.com/gcc-mirror/gcc/archive/gcc_5_1_0-release.tar.gz)

For our test compile, use this:

{% highlight bash linenos %}
mkdir hello \
cd hello \
echo -e "#include <iostream> int main() { printf(\"Hello world.\\\n\"); }" > hello.cpp \
cd -
{% endhighlight %}

As part of the Docker library, there is an actual GCC image, supported by the GCC development team. We'll be utilising GCC 5.1 as our compiler. Here is the command structure that will be used:

{% highlight bash linenos %}
docker run --rm \
-v /home/me/hello:/usr/src/hello \
-w /usr/src/hello gcc:5.1
{% endhighlight %}

To explain the Docker command, `run` runs a command in a new container, `--rm` removes the container after runtime, `-v /home/me/hello:/usr/src/hello` is the direct mount of our "hello" folder to the `/usr/src/hello` folder within the container, `-w /usr/src/hello` is our current working directory, and `gcc:5.1` is the image to use. While that is our Docker command, we aren't actually executing anything. If we want to compile our little application, it would be like so:

{% highlight bash linenos %}
docker run --rm \
-v /home/me/hello:/usr/src/hello \
-w /usr/src/hello gcc:5.1 \
gcc -o hello hello.cpp
{% endhighlight %}

That will perform the compilation, produce our binary, and then destroy the container. If you want to see the container, remove `--rm` and the container will persist, just remain exited. Looking at your folder structure, you should see the output of the GCC command, and now a binary, our hello application. To verify it works, execute it and you will see one of the most famous lines in all of computing.

While that is quite small, it's only a very mild compilation with no linked libraries or headers. Let's fix that by utilising some self-cannibalism, and compile GCC 5.1. You can either download it from the link above, or you can utilise it straight from Git:
{% highlight bash linenos %}
git clone https://github.com/gcc-mirror/gcc \
git pull origin --tags \
git checkout tags/gcc_5_1_0-release
{% endhighlight %}
While this can be done with one-time containers, I will utilise the underlying Linux filesystem of the container for my GCC build. This won't allow me to utilise it on OSX, but will allow me to still compile for Linux in a container. As I am utilising OS X to go through these commands, I can't utilise the Linux linkers. If you are utilising Linux, you can run the `./configure` script from your local directory.

To get the initial configuration, here is what we are using:

{% highlight bash linenos %}
docker run --rm \
-v /home/me/hello:/usr/src/gcc \
-w /usr/src/gcc lloydm/gccmp:5.1 \
./configure --disable-multilib
{% endhighlight %}

The Docker command is very similar to above, except we are in the GCC folder, and we are utilising an image I built instead of the library GCC image. We are using an image I built as GCC requires multi-precision libraries and flex. Both are on the library GCC image, however they are only used for the initial image building of GCC and are removed afterwords. My image is identical to the library image, only it doesn't remove either of those, which allows us to compile GCC. I'll go into the extensibility of this later. The `--disable-multilib` flag is set as I am on OS X, and I do not have the 32-bit headers, so Docker is running in only 64-bit mode, and that will allow me to properly compile GCC.

Now, let's compile GCC. This takes about 20-30 minutes on a 2.5 GHz Intel Core i7, so it will be awhile. The command is:

{% highlight bash linenos %}
docker run --rm \
-v /home/me/hello:/usr/src/gcc \
-w /usr/src/gcc lloydm/gccmp:5.1 \
make
{% endhighlight %}

20 or 30 minutes later, assuming there were no errors or meltdowns, you should have GCC compiled! If you run `make install` from your local terminal, it will install it locally. If you place `make install` at the end of a Docker command, it will install it to the container, so just be mindful of that. The library GCC image is only set to compile for C and C++, other languages will require different images.

So now that I've demonstrated the core concepts, let's take a minute and talk about extensibility. Some applications have outside requirements, i.e. GCC required flex and the multi-precision libraries. You can either edit the core Dockerfile to create a reusable image, as I did, or you can arbitraily install or make any required packages. For example, if I wanted to install the Python development headers in my GCC image, I could run `apt-get install python-dev` and now they are there. I personally preferred to create a new image and make it redistributable. Either way is fine, and really depends on what you want to do. As I had lots of testing, I utilised the removal of containers after I was done with them as much as possible, so I just modified the image for my own use.

While all of that is really fun and usefulf for compiling, those commands are long and have to be formatted for each location. So save some time on that, I came up with some syntatic sugar:

{% highlight bash linenos %}
docker run --rm \
-v "$PWD":/usr/src/"${PWD##*/}" \
-w /usr/src/"${PWD##*/}" gcc:latest
{% endhighlight %}

To explain, the container volume is intialised in your present working directory and is mounted as your current directory, changes to said directory, and then calls the image. If you want to get very fancy, you can utilise a Bash function (as I do):

{% highlight bash linenos %}
# persistent container
function dockercc() {
          docker run -v "$PWD":/usr/src/"${PWD##*/}" -w /usr/src/"${PWD##*/}" gcc:latest $@
}

#non-persistent container
function dockerncc() {
          docker run --rm -v "$PWD":/usr/src/"${PWD##*/}" -w /usr/src/"${PWD##*/}" gcc:latest $@
}
{% endhighlight %}

Now I can call `dockercc` for a persistent container with GCC, or `dockerncc` for a non-persistent container to compile with.

Good luck, and happy compiling!
