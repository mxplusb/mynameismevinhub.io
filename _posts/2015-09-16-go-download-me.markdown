---
layout: post
title: "Go Download Me"
date: Wed Sep 16 18:47:37 MST 2015
categories: golang
author: Mike Lloyd
comments: true
---

Recently I have been learning how to use Go, by Google. I think it is an interesting language, and I can definitely see it's benefits. I have been working
on a personal project involving Imgur, to run global statistics against the Imgur galleries. While progress is slow, I decided I wanted to refactor my
application from Python to Go, mostly for speed. What this means is I am learning Go, and how to best implement it with JSON and web development.

I decided that I would start with the JSON download, which is what I will discuss in this post.

{% highlight go linenos %}
func downloader(uri string, file string) {
}
{% endhighlight %}

Initially, I wasn't going to add arguments, I was going to embed the required downloads, but later I decided to implement the filename and URI strings
so I could reuse the function as much as I want. Extensibility? Most definitely, I'm a huge proponent of reusable code as much as possible. Part of this
is my likely my personal laziness, but I digress.

First order of business is creating the read-write file. By default, `os.Create()` does not set the executable bit, so files are read-write, but not executable.

{% highlight go linenos %}
func downloader(uri string, file string) {
	outFile, err := os.Create(file)
	if err != nil {
		panic(err)
	}
	defer outFile.Close()
}
{% endhighlight %}

First I am mapping the function to it's standard return, and then it's error handling. Because Go requires all variables to be in use, I decided to `panic()` instead
of utilising the [`_` variable](https://golang.org/doc/effective_go.html#blank), which is a discard function. This is just in case of an actual error, the function will panic and it's ended before it's completed. While I could write
`outFile.Close()` at the end of the function, Go's `defer()` functionality allows you to have a "Last In, First Out" featureset. We are deferring the file
closure now, mostly to prevent me from forgetting to close the file.

Next order of business is to create the HTTP client and request. A client is created as opposed to a straight request because (in my case)
custom headers are needed.

{% highlight go linenos %}
func downloader(uri string, file string) {
        outFile, err := os.Create(file)
	if err != nil {
		panic(err)
        }
	defer outFile.Close()

	client := &http.Client{}

	req, err := http.NewRequest("GET", uri, nil)
	req.Header.Add("Custom", "Header-Id a1b2c3d4")

	resp, err := client.Do(req)

	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()
}
{% endhighlight %}

Now, I will go through and explain some of what is added, as a lot just happened. First, `client := &http.Client{}` is a new instance of the the HTTP client
found in the standard library. The `&http.Client{}` is a [pointer](https://www.golang-book.com/books/intro/8) to the `http.Client{}` interface. This allows
us to store a key:value pair of whatever we need to for later (secret: our custom header). After the client has been created, we now can create a new request.
The `NewRequest` function takes an HTTP method, a URL, and an `io.Reader`. As we don't really need a reader, we set it to `nil` (`null` for Go). Next, our
custom header is added via the `Header.Add()` functionality. While I am not using my actual API key from Imgur, I am demoing how a header is added, the format
of which is `("HeaderKey", "HeaderValue")`. Now the request is properly formatted, the request is executed via `client.Do(req)`. `client.Do(req)` sends an HTTP
request and returns an HTTP response, following policy (e.g. redirects, cookies, auth), according to the [documentation](http://godoc.org/net/http#Client.Do).
After handling any potential errors, we defer the close of the request to after primary execution. As part of the `defer()` functionality, it's executed after
the previous defer statement.

If you've ever utilised the [requests library](http://www.python-requests.org/en/latest/) in Python, this will feel familiar to you. Now, the file has to be saved.

{% highlight go linenos %}
func downloader(uri string, file string) {
        outFile, err := os.Create(file)
	if err != nil {
		panic(err)
	}
	defer outFile.Close()

	client := &http.Client{}

	req, err := http.NewRequest("GET", uri, nil)
	req.Header.Add("Custom", "Header-Id a1b2c3d4")

	resp, err := client.Do(req)

	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()

	io.Copy(outFile, resp.Body)
}
{% endhighlight %}

At the end, we added `io.Copy(dest, source)` to our download function. That will take the URI and save the response body and save it to the destination file.
`io.Copy()` is utilised because it will move data through an iterative 32KB buffer from the source, to the destination. Now we have our downloader, but in
order to make this code useful, I have to wrap it around a command-line implementation. I'll add a couple flags, because why not?

{% highlight go linenos %}
func main() {
        uriPtr := flag.String("url", "URL", "the URL to be downloaded.")
	fileName := flag.String("file", "FILENAME", "the name of the file you want to save the data to.")

	flag.Parse()

	downloader(*uriPtr, *fileName)
}
{% endhighlight %}

Now for the main function, I add two command-line flags, one for the URL and one for the filename. `downloader(*uriPtr, *fileName)` is the function we just created, with the `*`
pointers to their [command line flags](https://gobyexample.com/command-line-flags). Now the utility is complete! It's not terribly complex, but it was definitely a learning
experience for me! Maybe down the road I'll implement concurrency or something cool.

For the rest of the code, you can find it on the [Gist](https://gist.github.com/f8b4295866980627547b), with logging enabled.
